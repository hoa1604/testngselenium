package libs;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

import objects.ExcelField;
import objects.Locator;

public class ExcelUtilsNew {
	private ExcelUtilsNew() {
	}

	private static ExcelUtilsNew instance;

	/**
	 * Gets the single instance of ExcelUtils.
	 *
	 * @return single instance of ExcelUtils
	 */
	public static synchronized ExcelUtilsNew getInstance() {
		if (instance == null) {
			instance = new ExcelUtilsNew();
		}
		return instance;
	}

	public JSONArray getJSONArrayFromExcel(Workbook excelWorkBook, String sheetName, List<ExcelField> columns) {
		System.out.println("at " + System.nanoTime() + " start getJSONArrayFromExcel ");
		JSONArray jsonArray = new JSONArray();
		try {
			Map<String, String> headerFieldMap = new HashMap<String, String>();
			for (ExcelField ExcelFieldDto : columns) {
				headerFieldMap.put(ExcelFieldDto.getHeaderName(), ExcelFieldDto.getFieldName());
			}
			// headerFieldMap: {REGULATOR_TYPE=regulatorType, PASSWORD=password,
			// USERNAME=username, TITLE=title, ID=id,
			// MIGRATION_VALIDATION=migrationValidation, URL=url}
			Sheet sheet = excelWorkBook.getSheet(sheetName);
			Iterator<Row> rowIterator = sheet.rowIterator();

			int rowNum = 0;
			Map<Integer, String> columnIndexMap = new HashMap<Integer, String>();
			while (rowIterator.hasNext()) {
				JSONObject jsonObject = new JSONObject();
				Row row = rowIterator.next();

				// Iterator<Cell> cellIterator = row.cellIterator();

				int columnIndex = 0;
				for (int i = 0; i < columns.size(); i++) {
					Cell cell = row.getCell(i);
					Object cellValue = "";
					if (cell != null) {
						cellValue = ExcelUtilsNew.getInstance().printCellValue(cell);
					} else {
						columnIndex++;
						continue;
					}

					if (rowNum == 0) {
						if (headerFieldMap.containsKey(cell.getRichStringCellValue().getString())) {
							columnIndexMap.put(columnIndex,
									headerFieldMap.get(cell.getRichStringCellValue().getString()));
						} else {
							columnIndexMap.put(columnIndex, cell.getRichStringCellValue().getString());
						}
					}
					jsonObject.put(columnIndexMap.get(columnIndex), cellValue);
					columnIndex++;
				}

				// if rownum == 0 jsonObject =
				// {"password":"PASSWORD","regulatorType":"REGULATOR_TYPE","migrationValidation":"MIGRATION_VALIDATION","id":"ID","title":"TITLE","url":"URL","username":"USERNAME"}
				// if rownum == 1 jsonObject =
				// {"password":"}V75nBXEaRgB","regulatorType":"CY","migrationValidation":"https://client-area-ui.staging.k8s.fxprimus.tech/en/dashboard","id":"LOG_001","title":"Check
				// client login to MA with regulator is
				// CY","url":"https://client-area-ui.staging.k8s.fxprimus.tech/en/login","username":"qa_automation+...@fxprimus.tech"}
				// put to json array if not header row
				if (rowNum > 0) {
					jsonArray.put(jsonObject);
				}

				rowNum++;
			}
			excelWorkBook.close();
		} catch (Exception ex) {
			System.err.println(ex.getMessage());
		}
		System.out.println("at " + System.nanoTime() + " end getJSONArrayFromExcel");
		return jsonArray;
	}

	public Object printCellValue(Cell cell) {
		Object result;
		switch (cell.getCellTypeEnum()) {
		case BOOLEAN:
			result = cell.getBooleanCellValue();
			break;
		case STRING:
			result = cell.getRichStringCellValue().getString();
			break;
		case NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				result = cell.getDateCellValue();
			} else {
				result = (long) cell.getNumericCellValue();
			}
			break;
		case FORMULA:
			result = cell.getCellFormula();
			break;
		case BLANK:
			result = "";
			break;
		default:
			result = "";
		}
		return result;
	}
}
