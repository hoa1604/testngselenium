package libs;

public class HeaderEnums {
	enum LocatorHeaderExcel {
		name, locator, type
	}

	public enum DataTestHeaderExcel {
		TestCaseName, OrderNo, OrderDate, CustomerName, OrderAmount, OrderStatus, OrderNoMessage, OrderDateMessage,
		CustomerNameMessage, OrderAmountMessage, OrderStatusMessage, OrderNoErrorMessage, OrderDateErrorMessage,
		CustomerNameErrorMessage, OrderAmountErrorMessage, OrderStatusErrorMessage,IsValidated, Status
	}
}