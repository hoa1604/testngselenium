package libs;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import TestNGSelenium.TestNGSelenium.properties.Environment;
import models.ExcelData;
import objects.ExcelItem;
import objects.ExcelRow;
import objects.Locator;

public class ExelUtils {
	private File file;
	private XSSFWorkbook workbook;
	private Sheet sheet;
	private List<List<ExcelItem>> data = new ArrayList<>();
	private List<Locator> locators = new ArrayList<>();
	private static String resultFile;

	public List<List<ExcelItem>> getData() {
		return data;
	}

	public void setData(List<List<ExcelItem>> data) {
		this.data = data;
	}

	public static String getResultFile() {
		return resultFile;
	}

	public void setResultFile(String resultFile) {
		this.resultFile = resultFile;
	}

	public List<Locator> getLocators() {
		return locators;
	}

	public void setLocators(List<Locator> locators) {
		this.locators = locators;
	}

	public ExelUtils() {

	}

	public void readLocatorFile(String path, String sheetName, int startRow) {
		openFile(path);
		getLocatorsFromExcel(sheetName, startRow);
	}

	public void readExcel(String path, String sheetName, int startRow) {
		copyFile(path);
		openFile(ExelUtils.resultFile);
		getObjectFromExel(sheetName, startRow);
	}

	public void openFile(String path) {
		try {
			FileInputStream fileInputStream = new FileInputStream(new File(path));
			file = new File(path);
			workbook = new XSSFWorkbook(fileInputStream);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void getObjectFromExel(String sheetName, int startRow) {
		sheet = workbook.getSheet(sheetName);
		int rows = sheet.getPhysicalNumberOfRows();
		Row HeaderRow = sheet.getRow(startRow - 1);
		for (Integer i = startRow; i < rows; i++) {
			List<ExcelItem> items = new ArrayList<>();
			Row currentRow = sheet.getRow(i);
			for (int j = 0; j < HeaderRow.getPhysicalNumberOfCells(); j++) {
				Cell currentCell = currentRow.getCell(j);
				String header = HeaderRow.getCell(j).getStringCellValue();
				String value = readCellValue(currentCell);
				items.add(new ExcelItem(i, j, header, value));
			}
			data.add(items);
		}
	}

	public void getLocatorsFromExcel(String sheetName, int startRow) {
		sheet = workbook.getSheet(sheetName);
		int rows = sheet.getPhysicalNumberOfRows();
		Row HeaderRow = sheet.getRow(startRow - 1);
		for (int i = startRow; i < rows; i++) {
			Locator locator = new Locator();
			Row currentRow = sheet.getRow(i);
			for (int j = 0; j < HeaderRow.getPhysicalNumberOfCells(); j++) {
				Cell currentCell = currentRow.getCell(j);
				String header = HeaderRow.getCell(j).getStringCellValue().toLowerCase();
				if (HeaderEnums.LocatorHeaderExcel.locator.toString().contains(header))
					locator.setLocator(readCellValue(currentCell));
				else if (HeaderEnums.LocatorHeaderExcel.type.toString().contains(header))
					locator.setType(readCellValue(currentCell));
				else if (HeaderEnums.LocatorHeaderExcel.name.toString().contains(header))
					locator.setName(readCellValue(currentCell));
			}
			locators.add(locator);
		}
	}

	public void updateExel(String value, int row, int cell) {
		Cell currentCell = sheet.getRow(row).getCell(cell);
		if(currentCell==null)
			currentCell= sheet.getRow(row).createCell(cell);
		currentCell.setCellValue(value);
	}

	public void saveExel() {
		FileOutputStream out;
		try {
			out = new FileOutputStream(file);
			workbook.write(out);
			out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private String readCellValue(Cell cell) {
		String value = null;
		if (cell != null)
			switch (cell.getCellTypeEnum()) {
			case _NONE:
				break;
			case BOOLEAN:
				value = String.valueOf(cell.getBooleanCellValue());
				break;
			case BLANK:

				break;
			case FORMULA:
				FormulaEvaluator evaluator = workbook.getCreationHelper().createFormulaEvaluator();
				value = String.valueOf(evaluator.evaluate(cell).getNumberValue());
				break;
			case NUMERIC:
				value = String.valueOf(cell.getNumericCellValue());
				break;
			case STRING:
				value = cell.getStringCellValue();
				break;
			case ERROR:
				value = "!";
				break;
			default:
				break;

			}
		return value;

	}

	public void copyFile(String sourcePath) {
		FileInputStream excelFile;
		try {
			String projectPath = System.getProperty("user.dir");
			// result file
			Date date = new Date();
			SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyhhmmss");
			String strDate = formatter.format(date);
			String resultFileName = "CreateOrderTestResult_" + strDate + ".xlsx";
			String destinationPath = projectPath
					+ MessageFormat.format("{0}/{1}", Environment.getProperties().dataTestResultFolder(), resultFileName);
			// copy
			excelFile = new FileInputStream(new File(sourcePath));
			Workbook workbook = new XSSFWorkbook(excelFile);
			FileOutputStream outputStream = new FileOutputStream(destinationPath);
			workbook.write(outputStream);
			workbook.close();
			this.resultFile = destinationPath;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
