package TestNGSelenium.TestNGSelenium.pages;

import static org.assertj.core.api.Assertions.assertThat;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import TestNGSelenium.TestNGSelenium.properties.Environment;
import io.qameta.allure.Step;

public class DemoGuruHomePage extends AbstractPage<DemoGuruHomePage> {

	@FindBy(id = "site-name")
	private WebElement sideName;

	public DemoGuruHomePage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}

	@Step("Open the demo page")
	@Override
	protected void load() {
		driver.get(Environment.getProperties().demourl());
	}

	@Override
	protected void isLoaded() throws Error {
		System.out.println("driver.getTitle: " + driver.getTitle());
		assertThat(driver.getTitle()).isEqualTo("Welcome: Mercury Tours");
	}

	@Step("Get side name")
	public String getSloganText() {
		System.out.println("Get side name");
		return sideName.getText();
	}
}
