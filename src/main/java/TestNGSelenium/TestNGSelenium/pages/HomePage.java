package TestNGSelenium.TestNGSelenium.pages;

import io.qameta.allure.Step;
import libs.ExelUtils;
import objects.ExcelItem;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.annotations.BeforeClass;

import TestNGSelenium.TestNGSelenium.properties.Environment;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

public class HomePage extends AbstractPage<HomePage> {
	
    @FindBy(className = "h000-mktg")
    private WebElement slogan;

	public HomePage(WebDriver driver) {
        super(driver);
    }

    @Step("Open the Github homepage")
    @Override
    protected void load() {
        driver.get(Environment.getProperties().url());
    }

    @Override
    protected void isLoaded() throws Error {
    	System.out.println("driver.getTitle: "+driver.getTitle());
        assertThat(driver.getTitle()).isEqualTo("GitHub: Where the world builds software · GitHub");
    }
    
    @Step("Get slogan text")
    public String getSloganText() {
    	System.out.println("Get slogan text");
        return slogan.getText();
    }

  
}
