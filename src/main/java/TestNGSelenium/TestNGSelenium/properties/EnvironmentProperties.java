package TestNGSelenium.TestNGSelenium.properties;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources("file:src/main/resources/${env}_environment.properties" )
public interface EnvironmentProperties extends Config {
    String url();

    String demourl();

    String username();

    String password();
    
    String dataTestFolder();
    
    String dataTestResultFolder();
    
    String locatorFolder();
    
    String reportFolder();
    
    String screenShotFolder();
}
