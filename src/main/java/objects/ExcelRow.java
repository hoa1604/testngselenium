package objects;

import java.util.List;

public class ExcelRow {
	private static List<ExcelItem> row;

	public List<ExcelItem> getRow() {
		return row;
	}

	public void setRow(List<ExcelItem> row) {
		this.row = row;
	}
	
	public static ExcelItem findExcelItem(String name) {
		return row.stream().filter(item -> name.equals(item.getHeader())).findFirst().orElse(null);
	}
	
	public static String getValue(String name) {
		ExcelItem item = findExcelItem(name);
		return item.getValue();
	}

}
