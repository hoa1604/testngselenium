package objects;

import java.util.List;

public class ExcelItem {
	private int row;
	private int column;
	private String header;
	private String value;

	public ExcelItem(int row, int column, String header, String value) {
		this.row = row;
		this.column = column;
		this.header = header;
		this.value = value;
	}

	public int getRow() {
		return row;
	}

	public void setRow(int row) {
		this.row = row;
	}

	public int getColumn() {
		return column;
	}

	public void setColumn(int column) {
		this.column = column;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public static ExcelItem findExcelItem(List<ExcelItem> items, String name) {
		return items.stream().filter(item -> name.equals(item.getHeader())).findFirst().orElse(null);
	}

	public static String getValue(List<ExcelItem> items, String name) {
		ExcelItem item = findExcelItem(items, name);
		if (item != null)
			return item.getValue();
		return null;
	}

	@Override
	public String toString() {

		return "Row :" + this.row + "; Column: " + this.column + "; Header: " + this.header + "; Value: " + this.value;
	}

}
