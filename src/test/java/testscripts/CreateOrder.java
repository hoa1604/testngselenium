package testscripts;

import static org.testng.Assert.assertEquals;

import java.io.IOException;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import TestNGSelenium.TestNGSelenium.properties.Environment;
import dataprovider.ExcelDataProvider;
import libs.HeaderEnums.DataTestHeaderExcel;
import libs.Utility;
import objects.ExcelItem;
import pages.CreateOrderPage;

public class CreateOrder extends ExcelDataProvider {
	private WebDriver driver;
	private CreateOrderPage page;
	String reportPath = MessageFormat.format("{0}/create_order.html", Environment.getProperties().reportFolder());
	ExtentHtmlReporter reporter = new ExtentHtmlReporter(reportPath);
	ExtentReports extent = new ExtentReports();
	ExtentTest logger;
	String testCaseName;
	String locatorSheetName = "CreatePage";

	@BeforeMethod
	public void setup() {
		String projectPath = System.getProperty("user.dir");
		System.setProperty("webdriver.chrome.driver", projectPath + "/src/test/resources/drivers/chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		page = new CreateOrderPage(driver, locatorSheetName);
		driver.navigate().to("https://pdocrud.com/demo/pages/one-page-template");
		extent.attachReporter(reporter);

	}

	@Test(dataProvider = "create-data-provider", dataProviderClass = ExcelDataProvider.class)
	public void createCustomer(List<ExcelItem> data) throws InterruptedException {
		ExcelItem statusCell = ExcelItem.findExcelItem(data, DataTestHeaderExcel.Status.toString());
		String isValidated = ExcelItem.getValue(data, DataTestHeaderExcel.IsValidated.toString());
		testCaseName = ExcelItem.getValue(data, DataTestHeaderExcel.TestCaseName.toString());
		logger = extent.createTest("create order: " + testCaseName);

		logger.log(Status.INFO, "enter value");

		String orderNo = ExcelItem.getValue(data, DataTestHeaderExcel.OrderNo.toString());
		page.enterOrderNo(orderNo);
		if ((orderNo == null || orderNo.isEmpty()) && isValidated != null && !isValidated.isEmpty()) {
			logger.log(Status.INFO, "exist input");
			page.validateOrderNo();
			logger.log(Status.INFO, "validate");
			validateOrderNoErrorMessage(data, statusCell);
			logger.log(Status.PASS, "pass");
			exelUtils.updateExel("Pass", statusCell.getRow(), statusCell.getColumn());
			return;
		}

		String orderDate = ExcelItem.getValue(data, DataTestHeaderExcel.OrderDate.toString());
		page.enterOrderDate(orderDate);
		if ((orderDate == null || orderDate.isEmpty()) && isValidated != null && !isValidated.isEmpty()) {
			logger.log(Status.INFO, "exist input");
			page.validateOrderDate();
			logger.log(Status.INFO, "validate");
			validateOrderDataErrorMessage(data, statusCell);
			logger.log(Status.PASS, "pass");
			exelUtils.updateExel("Pass", statusCell.getRow(), statusCell.getColumn());
			return;
		}

		String customerName = ExcelItem.getValue(data, DataTestHeaderExcel.CustomerName.toString());
		page.enterCustomerName(customerName);
		if ((customerName == null || customerName.isEmpty()) && isValidated != null && !isValidated.isEmpty()) {
			logger.log(Status.INFO, "exist input");
			page.validateCustomerName();
			logger.log(Status.INFO, "validate");
			validateCustomerNameErrorMessage(data, statusCell);
			logger.log(Status.PASS, "pass");
			exelUtils.updateExel("Pass", statusCell.getRow(), statusCell.getColumn());
			return;
		}

		String orderAmount = ExcelItem.getValue(data, DataTestHeaderExcel.OrderAmount.toString());
		page.enterOrderAmount(orderAmount);
		if ((orderAmount == null || orderAmount.isEmpty()) && isValidated != null && !isValidated.isEmpty()) {
			logger.log(Status.INFO, "exist input");
			page.validateOrderAmount();
			logger.log(Status.INFO, "validate");
			validateOrderAmountErrorMessage(data, statusCell);
			logger.log(Status.PASS, "pass");
			exelUtils.updateExel("Pass", statusCell.getRow(), statusCell.getColumn());
			return;
		}

		String orderStatus = ExcelItem.getValue(data, DataTestHeaderExcel.OrderStatus.toString());
		page.enterOrderStatus(orderStatus);
		if ((orderStatus == null || orderStatus.isEmpty()) && isValidated != null && !isValidated.isEmpty()) {
			logger.log(Status.INFO, "exist input");
			page.validateOrderStatus();
			logger.log(Status.INFO, "validate");
			validateOrderStatusErrorMessage(data, statusCell);
			logger.log(Status.PASS, "pass");
			exelUtils.updateExel("Pass", statusCell.getRow(), statusCell.getColumn());
			return;
		}

		logger.log(Status.INFO, "click save button");
		page.clickSaveButton();

		logger.log(Status.INFO, "validate");
		// order no
		validateOrderNoErrorMessage(data, statusCell);
		// order date
		validateOrderDataErrorMessage(data, statusCell);
		// customer name
		validateCustomerNameErrorMessage(data, statusCell);
		// order amount
		validateOrderAmountErrorMessage(data, statusCell);
		// order status
		validateOrderStatusErrorMessage(data, statusCell);

		exelUtils.updateExel("Pass", statusCell.getRow(), statusCell.getColumn());
		logger.log(Status.PASS, "pass");

	}

	@AfterMethod()
	public void tearDown(ITestResult result) throws IOException {
		if (result.getStatus() == ITestResult.FAILURE) {
			String screenShotPath = takeScreenShot();
			logger.fail(result.getThrowable().getMessage(),
					MediaEntityBuilder.createScreenCaptureFromPath(screenShotPath).build());
		}
		extent.flush();

		driver.close();
		driver.quit();
	}

	@AfterClass
	public void saveExcel() {
		System.out.println("saveExcel");
		exelUtils.saveExel();
		System.out.println("done");
	}

	private void validateOrderStatusErrorMessage(List<ExcelItem> data, ExcelItem statusCell) throws AssertionError {
		ExcelItem orderStatusMessageCell = ExcelItem.findExcelItem(data,
				DataTestHeaderExcel.OrderStatusMessage.toString());
		String orderStatusMessageExpected = orderStatusMessageCell.getValue();

		String orderStatusMessageActual = page.getOrderStatusError();

		assertAndWriteResult(data, orderStatusMessageExpected, orderStatusMessageActual,
				DataTestHeaderExcel.OrderStatusErrorMessage.toString(), statusCell);
	}

	private void validateOrderAmountErrorMessage(List<ExcelItem> data, ExcelItem statusCell) throws AssertionError {
		ExcelItem orderAmountMessageCell = ExcelItem.findExcelItem(data,
				DataTestHeaderExcel.OrderAmountMessage.toString());
		String orderAmountMessageExpected = orderAmountMessageCell.getValue();

		String orderAmountMessageActual = page.getOrderAmountError();

		assertAndWriteResult(data, orderAmountMessageExpected, orderAmountMessageActual,
				DataTestHeaderExcel.OrderAmountErrorMessage.toString(), statusCell);
	}

	private void validateCustomerNameErrorMessage(List<ExcelItem> data, ExcelItem statusCell) throws AssertionError {
		ExcelItem customerNameMessageCell = ExcelItem.findExcelItem(data,
				DataTestHeaderExcel.CustomerNameMessage.toString());
		String customerNameMessageExpected = customerNameMessageCell.getValue();

		String customerNameMessageActual = page.getCustormerNameError();

		assertAndWriteResult(data, customerNameMessageExpected, customerNameMessageActual,
				DataTestHeaderExcel.CustomerNameErrorMessage.toString(), statusCell);
	}

	private void validateOrderDataErrorMessage(List<ExcelItem> data, ExcelItem statusCell) throws AssertionError {
		ExcelItem orderDateMessageCell = ExcelItem.findExcelItem(data, DataTestHeaderExcel.OrderDateMessage.toString());
		String orderDateMessageExpected = orderDateMessageCell.getValue();

		String orderDateMessageActual = page.getOrderDateError();

		assertAndWriteResult(data, orderDateMessageExpected, orderDateMessageActual,
				DataTestHeaderExcel.OrderDateErrorMessage.toString(), statusCell);
	}

	private void validateOrderNoErrorMessage(List<ExcelItem> data, ExcelItem statusCell) throws AssertionError {
		ExcelItem orderNoMessageCell = ExcelItem.findExcelItem(data, DataTestHeaderExcel.OrderNoMessage.toString());
		String orderNoMessageExpected = orderNoMessageCell.getValue();

		String orderNoMessageActual = page.getOrderNoError();

		assertAndWriteResult(data, orderNoMessageExpected, orderNoMessageActual,
				DataTestHeaderExcel.OrderNoErrorMessage.toString(), statusCell);
	}

	private void assertAndWriteResult(List<ExcelItem> data, String messageExpected, String messageActual, String header,
			ExcelItem statusCell) throws AssertionError {
		try {
			assertEquals(messageActual, messageExpected);
		} catch (AssertionError e) {
			ExcelItem actualMessageCell = ExcelItem.findExcelItem(data, header);
			exelUtils.updateExel(messageActual, actualMessageCell.getRow(), actualMessageCell.getColumn());
			exelUtils.updateExel("Fail", statusCell.getRow(), statusCell.getColumn());
			throw e;
		}
	}

	private String takeScreenShot() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("ddMMyyyyhhmmss");
		String strDate = formatter.format(date);
		String imageName = testCaseName + "_" + strDate + ".png";
		return Utility.getScreenShot(driver, imageName);
	}

}
