package dataprovider;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.DataProvider;

import TestNGSelenium.TestNGSelenium.properties.Environment;
import libs.ExelUtils;
import objects.ExcelItem;
import objects.ExcelRow;

public class ExcelDataProvider {
	protected static ExelUtils exelUtils = new ExelUtils();
	@DataProvider (name = "create-data-provider")
	public static Object[] getCreateData() {
		String projectPath = System.getProperty("user.dir");
		String dataFilePath =projectPath + MessageFormat.format("{0}/OrderData.xlsx", Environment.getProperties().dataTestFolder());
		exelUtils.readExcel(dataFilePath, "CreateAccount", 2);
		List<List<ExcelItem>> items = exelUtils.getData();
		return items.toArray();
	}

}
