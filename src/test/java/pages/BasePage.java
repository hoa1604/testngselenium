package pages;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BasePage extends PageLoadProperties {
	public BasePage(String sheetName) {
		super(sheetName);
	}
	protected WebDriver driver;

	
	@BeforeSuite
	public void setupSuite() {
		WebDriverManager.chromedriver().setup();
	}

	@BeforeClass
	public void setup() {
		driver = new ChromeDriver();
	}

	@AfterClass
	public void teardown() {
		driver.quit();
	}

	public void sendKey(String locator, String value) {
		driver.findElement(PageLoadProperties.getElement(locator)).sendKeys(value);
	}

	public void click(String locator) {
		driver.findElement(PageLoadProperties.getElement(locator)).click();
	}

	public void submit(String locator) {
		driver.findElement(PageLoadProperties.getElement(locator)).submit();
	}

	public String getText(String locator) {
		return driver.findElement(PageLoadProperties.getElement(locator)).getText();
	}

	public boolean isExisted(String locator) {
		By by = PageLoadProperties.getElement(locator);
		driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
		try {
			driver.findElement(by);
			return true;
		} catch (NoSuchElementException e) {
			return false;
		} finally {
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		}
	}

}
