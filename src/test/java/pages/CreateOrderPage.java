package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CreateOrderPage extends BasePage {
	public CreateOrderPage(WebDriver driver, String locatorSheetName) {
		super(locatorSheetName);
		super.driver = driver;
	}

	public void enterOrderNo(String value) {
		if (value != null && !value.isEmpty())
			sendKey("txtOrderNo", value);
	}

	public void validateOrderNo() {
		sendKey("txtOrderNo", "\t");
	}

	public String getOrderNoError() {
		if (isExisted("orderNoError"))
			return getText("orderNoError");
		else
			return null;
	}

	public void enterOrderDate(String value) {
		if (value != null && !value.isEmpty())
			sendKey("dtOrderDate", value);
		sendKey("dtOrderDate", "\t");
	}

	public void validateOrderDate() {
		sendKey("dtOrderDate", "\t");
	}
	
	public String getOrderDateError() {
		if (isExisted("orderDateError"))
			return getText("orderDateError");
		else
			return null;
	}

	public void enterCustomerName(String value) {
		if (value != null && !value.isEmpty())
			sendKey("txtCustormerName", value);
	}
	
	public void validateCustomerName() {
		sendKey("txtCustormerName", "\t");
	}
	
	public String getCustormerNameError() {
		if (isExisted("customerNameError"))
			return getText("customerNameError");
		else
			return null;
	}

	public void enterOrderAmount(String value) {
		if (value != null && !value.isEmpty())
			sendKey("txtOrderAmount", value);
	}

	public void validateOrderAmount() {
		sendKey("txtOrderAmount", "\t");
	}
	
	public String getOrderAmountError() {
		if (isExisted("orderAmountError"))
			return getText("orderAmountError");
		else
			return null;
	}

	public void enterOrderStatus(String value) {
		if (value != null && !value.isEmpty())
			sendKey("txtOrderStatus", value);
	}

	public void validateOrderStatus() {
		sendKey("txtOrderStatus", "\t");
	}
	
	public String getOrderStatusError() {
		if (isExisted("orderStatusError"))
			return getText("orderStatusError");
		else
			return null;
	}

	public void clickSaveButton() {
		submit("btnSave");
	}
}
