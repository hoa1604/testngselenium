package pages;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.google.common.base.Enums;

import TestNGSelenium.TestNGSelenium.properties.Environment;
import libs.CommonEnums;
import libs.ExelUtils;
import objects.ExcelItem;
import objects.Locator;

public class PageLoadProperties {

	protected static ExelUtils exelUtils = new ExelUtils();

	protected static List<Locator> locators = new ArrayList<>();

	public PageLoadProperties(String sheetName) {
		String projectPath = System.getProperty("user.dir");
		String dataFilePath = projectPath
				+ MessageFormat.format("{0}/locators.xlsx", Environment.getProperties().locatorFolder());
		exelUtils.readLocatorFile(dataFilePath, sheetName, 1);
		locators = exelUtils.getLocators();
	}

	public static Locator findLocator(String name) {
		return locators.stream().filter(locator -> name.equals(locator.getName())).findFirst().orElse(null);
	}

	public static By getElement(String name) {
		Locator locator = findLocator(name);
		String type = locator.getType().toLowerCase();
		if (CommonEnums.LocatorType.classname.toString().equals(type)) {
			return By.className(locator.getLocator());
		} else if (CommonEnums.LocatorType.xpath.toString().equals(type)) {
			return By.xpath(locator.getLocator());
		} else if (CommonEnums.LocatorType.id.toString().equals(type)) {
			return By.id(locator.getLocator());
		}
		return null;
	}

}
