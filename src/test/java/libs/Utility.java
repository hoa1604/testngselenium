package libs;

import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import TestNGSelenium.TestNGSelenium.properties.Environment;

public class Utility {

	public static String getScreenShot(WebDriver driver, String fileName) {
		TakesScreenshot ts = (TakesScreenshot) driver;

		File src = ts.getScreenshotAs(OutputType.FILE);

		String projectPath = System.getProperty("user.dir");
		String path = projectPath
				+ MessageFormat.format("{0}/{1}", Environment.getProperties().screenShotFolder(), fileName);
		File destination = new File(path);

		try {
			FileUtils.copyFile(src, destination);
		} catch (IOException e) {
			System.out.println("Capture failed: " + e.getMessage());
		}

		return path;
	}
}
