package TestNGSelenium.TestNGSelenium.testscripts;

import TestNGSelenium.TestNGSelenium.pages.DemoGuruHomePage;
import TestNGSelenium.TestNGSelenium.pages.HomePage;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Test
public class DemoGuruTest extends TestBase {

    public void verifySideName() {
        DemoGuruHomePage demoPage = new DemoGuruHomePage(driver);
        demoPage.get();
        System.out.println("homePage.getSloganText(): "+demoPage.getSloganText());
        assertThat(demoPage.getSloganText()).isEqualTo("Demo Site");
    }
}
