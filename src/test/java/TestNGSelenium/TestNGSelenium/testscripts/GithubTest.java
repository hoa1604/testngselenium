package TestNGSelenium.TestNGSelenium.testscripts;

import TestNGSelenium.TestNGSelenium.pages.HomePage;
import libs.ExelUtils;
import objects.ExcelItem;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.HashMap;
import java.util.List;


public class GithubTest extends TestBase {
	static ExelUtils exelUtils = new ExelUtils();

//	@BeforeClass
//	public static void initExel() {
//		String projectPath = System.getProperty("user.dir");
//		String dataFilePath = projectPath + "/src/main/resources/data/locators.xlsx";
//		exelUtils.readExcel(dataFilePath, "Login", 1);
//		HashMap<String, List<ExcelItem>> items = exelUtils.getData();
//		 items.forEach((k, v) -> {
//	            System.out.format("key: %s, value: %d%n", k, v);
//	        });
//		
//	}
	
	@Test
	public void verifyGithubSlogan() {
		HomePage homePage = new HomePage(driver);
		homePage.get();
		System.out.println("homePage.getSloganText(): " + homePage.getSloganText());
		assertThat(homePage.getSloganText()).isEqualTo("Built for developers");
	}
	
	

}
